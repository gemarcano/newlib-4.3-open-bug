.PHONY: clean all

all: main

main:
	arm-none-eabi-gcc main.c -o main --specs=nano.specs --specs=nosys.specs
clean:
	rm -f main
